<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="/resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<script src="/resources/js/dk.js"></script>
<script>
	
</script>
</head>
<body>

	<div style="text-align: center;" class="form-group">
		<img width="200" height="50" class=""
			src="https://papago.naver.com/97ec80a681e94540414daf2fb855ba3b.svg"
			alt="PAPAGO PC용" style="display: inline;" />
		<!-- <h1 style="display: inline;">Naver Papago</h1> -->
	</div>

	<div style="width: 100%; height: 500px; text-align: center; margin: 10 10 10 10" class="form-group">
		<div id="leftDiv" class="form-group">
			<div class="selectDiv">
				<label for="srcText">번역 전</label>
				<select class="select" id="srcLang"></select>
				<button class="btn btn-success" id="btnTrans" onclick="doTrans()">번역하기</button>
			</div>
			<textarea class="form-control z-depth-1" rows="20" cols="40" id="srcText"></textarea>
		</div>
			
		<div id="rightDiv" class="form-group">
			<div class="selectDiv">
				<label for="resultText">번역 후</label>
				<select class="select" id="resLang"></select>
				<button class="btn btn-success" onclick="doList()">조회하기</button>
			</div>
			<textarea class="form-control z-depth-1" rows="20" cols="40" id="resultText"></textarea>
		</div>
	</div> 

<!-- <div class="form-group shadow-textarea">
	<div class="labelDiv"><label for="exampleFormControlTextarea6">번역 전</label><select class="select" id="srcLang"></select></div>
	<textarea class="form-control z-depth-1" id="srcText" rows="30" placeholder="Write something here..."></textarea>
  	<button class="btn" onclick="doTrans()">번역</button>
  	<div class="labelDiv"><label for="exampleFormControlTextarea6">번역 후</label><select class="select" id="resLang"></select></div>
  	<textarea class="form-control z-depth-1" id="resultText" rows="30" placeholder="Write something here..."></textarea>
</div>

<div class="form-group shadow-textarea">
  
</div> -->
	
	<div class="form-group" id="tableDiv">
		<table border="1">
			<tbody id="tBody">
			</tbody>
		</table>
	</div>
</body>
</html>