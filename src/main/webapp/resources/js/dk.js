
	var flag = false;

	function doTrans(){
		var src = document.querySelector('#srcLang');
		var trg = document.querySelector('#resLang');
		var text = document.querySelector('#srcText');
		var url = '/translation/' + trg.value + '/' + src.value + '/';
		url += text.value;
		//url += encodeURI(text.value);
		console.log(url);
		var xhr = new XMLHttpRequest();
		xhr.open('GET',url);
		xhr.onreadystatechange = function(){
			if(xhr.readyState==xhr.DONE && xhr.status==200){
				console.log(xhr.response);
				var res = JSON.parse(xhr.response);
				console.log(res);
				console.log(res.TH_TGTEXT);
				var result = document.querySelector('#resultText');
				result.value = res.TH_TGTEXT;
			}
		}
		xhr.send();
		if(flag==true){
			doList();	
		}
	}
	
	function addLangList(){
		var html='';
		var url = '/langlist';
		var xhr = new XMLHttpRequest();
		xhr.open('GET',url);
		xhr.onreadystatechange = function(){
			if(xhr.readyState==xhr.DONE && xhr.status==200){
				console.log(xhr.response);
				var res = JSON.parse(xhr.response);
				console.log(res);
				for(var re of res){
					var tlCode = re.TL_CODE.trim();
					console.log('tlCode : ' + tlCode);
					var tlLang = re.TL_LANG;
					console.log('tlLang : ' + tlLang);
					
					html += '<option value="' + tlCode + '">' + tlLang + '</option>';
				}
				
			}
			console.log('html : ' + html);
			document.querySelector('#srcLang').innerHTML = html;
			document.querySelector('#resLang').innerHTML = html;
		}
		xhr.send();
	}
	addLangList();
	
	
	
	function doList(){
		var html = '';
		var url = '/translations';
		var xhr = new XMLHttpRequest();
		xhr.open('GET',url,true);
		xhr.onreadystatechange = function(){
			if(xhr.readyState==4 && xhr.status==200){
				console.log(xhr.response);
				var res = JSON.parse(xhr.response);
				console.log(res);
				/*
				<tr>
					<th>랭킹</th>
					<th>번호</th>
					<th>소스</th>
					<th>타겟</th>
					<th>소스내용</th>
					<th>타겟내용</th>
					<th>검색횟수</th>
					<th>에러메세지</th>
				</tr>
				*/
				html += '<tr><th>랭킹</th><th>번호</th><th>소스</th><th>타겟</th><th>소스내용</th><th>타겟내용</th><th>검색횟수</th><th>에러메세지</th></tr>';
					
				for(var i=0;i<res.length;i++){
					html += '<tr>';
					html += '<td>' + (i+1) + '</td>'; 
					html += '<td>' + res[i].TH_NUM + '</td>';
					html += '<td>' + res[i].TH_SOURCE + '</td>';
					html += '<td>' + res[i].TH_TARGET + '</td>';
					html += '<td>' + res[i].TH_SRCTEXT + '</td>';
					html += '<td>' + res[i].TH_TGTEXT + '</td>';
					html += '<td>' + res[i].TH_COUNT + '</td>';
					if(res[i].TH_ERROR!=undefined){
						html += '<td>' + res[i].TH_ERROR + '</td>';	
					}else{
						html += '<td>-</td>';
					}
					html += '</tr>';
				}
				/* for(var re of res){
					html += '<tr>';
					html += '<td>' + (i++) + '</td>'; 
					html += '<td>' + re.TH_NUM + '</td>';
					html += '<td>' + re.TH_SOURCE + '</td>';
					html += '<td>' + re.TH_TARGET + '</td>';
					html += '<td>' + re.TH_SRCTEXT + '</td>';
					html += '<td>' + re.TH_TGTEXT + '</td>';
					html += '<td>' + re.TH_COUNT + '</td>';
					if(re.TH_ERROR!=undefined){
						html += '<td>' + re.TH_ERROR + '</td>';	
					}else{
						html += '<td>-</td>';
					}
					html += '</tr>';
				} */
				html += '<button onclick="doCloseList()">접기</button>'
			}
			document.querySelector('#tBody').innerHTML = html;
			flag = true;
			/* if(flag!=true){
				document.querySelector('#tBody').innerHTML = html;
				flag = true;
			}else{
				document.querySelector('#tBody').innerHTML = '';
				flag = false;
			} */
		}
		xhr.send();
	}
	
	function doCloseList(){
		var html ='';
		document.querySelector('#tBody').innerHTML = html;
		flag = false;
	}
	
	