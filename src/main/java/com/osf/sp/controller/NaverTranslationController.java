package com.osf.sp.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.io.JsonStringEncoder;
import com.osf.sp.dao.NaverTranslationDAO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class NaverTranslationController {

	@Resource
	private NaverTranslationDAO ntdao = new NaverTranslationDAO();
	
	@GetMapping("/langlist")
	public @ResponseBody List<Map<String,Object>> getTransLangs(){
		return ntdao.selectTransLangList();
	}
	
	@CrossOrigin("*")
	@GetMapping("/translations")
	public @ResponseBody List<Map<String,Object>> getTranslations(){
		List<Map<String,Object>> rMaps = ntdao.selectTranslationHisList();
		for(Map<String,Object> rMap : rMaps) {
			if(rMap.get("TH_TGTEXT") instanceof Clob) {
				rMap.put("TH_TGTEXT", clobToString((Clob)rMap.get("TH_TGTEXT")));
			}
			//rMaps.add(rMap);
			log.info("rMaps => {}",rMaps);
		}
		return rMaps;
//		return ntdao.selectTranslationHisList();
	}
	
	@GetMapping("/translation/{target}/{source}/{text}")
	public @ResponseBody Object doTranslation(
			@PathVariable("target") String target,
			@PathVariable("source") String source,
			@PathVariable("text") String text,
			Model model
			)  {
		log.info("target=>{},source=>{},text=>{}",new String[] {target,source,text});
		Map<String,String> param = new HashMap<String,String>();
		param.put("target", target);
		param.put("source", source);
		param.put("text", text);
		Map<String,Object> rMap = ntdao.selectTranslationHisOne(param);
		log.info("rMap_1 => {}",rMap);
		if(rMap==null) {
			rMap = translationTest(param);
			log.info("rMap_trans => {}",rMap);
			if(rMap.get("errorCode")!=null) {
				param.put("error",rMap.get("errorCode").toString());
				param.put("TH_TGTEXT",rMap.get("errorCode").toString());
			}else {
				param.put("error", "");
				param.put("TH_TGTEXT",((Map)((Map)rMap.get("message")).get("result")).get("translatedText").toString());
				log.info("translatedText => {}",((Map)((Map)rMap.get("message")).get("result")).get("translatedText").toString());
			}
			
			ntdao.insertTranslationHisOne(param);
			rMap = ntdao.selectTranslationHisOne(param);
			log.info("rMap_2 => {}",rMap);
			
		}else {
			log.info("is this update");
			ntdao.updateCountOne(rMap);
			log.info("rMap_update => {}",rMap);
		}
		log.info("rMap_3 => {}",rMap);
		if(rMap.get("TH_TGTEXT") instanceof Clob) {
			rMap.put("TH_TGTEXT", clobToString((Clob)rMap.get("TH_TGTEXT")));
		}
		log.info("rMap_return => {}",rMap);
		return rMap;
		
//		resultText = getTranslatedText((String)translationTest(target, source, text));
//		param.put("tgtext",resultText);
//		model.addAttribute("tgtext", resultText);
//		return param;
	
	}
	
	private String clobToString(Clob data) {
	    StringBuilder sb = new StringBuilder();
	    try {
	        Reader reader = data.getCharacterStream();
	        BufferedReader br = new BufferedReader(reader);

	        String line;
	        while(null != (line = br.readLine())) {
	            sb.append(line);
	        }
	        br.close();
	    } catch (SQLException e) {
	    } catch (IOException e) {
	    }
	    return sb.toString();
	}
	
	private Map<String,Object> translationTest(Map<String,String> param) {
		String clientId = "bW29mxN17qCeRXGSallE";//애플리케이션 클라이언트 아이디값";
        String clientSecret = "3Qdwv7AfGY";//애플리케이션 클라이언트 시크릿값";
        String text = param.get("text");
        String source = param.get("source");
        String target = param.get("target");
        try {
            text = URLEncoder.encode(text, "UTF-8");
            String apiURL = "https://openapi.naver.com/v1/papago/n2mt";
            URL url = new URL(apiURL);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("X-Naver-Client-Id", clientId);
            con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
            // post request
            String postParams = "source=" + source + "&target=" + target + "&text=" + text;
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();
            int responseCode = con.getResponseCode();
            BufferedReader br;
            if(responseCode==200) { // 정상 호출
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {  // 에러 발생
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = br.readLine()) != null) {
                response.append(inputLine);
                System.out.println("inputLine : " + inputLine);
            }
            ObjectMapper om = new ObjectMapper();
            br.close();
            Map<String,Object> rMap = om.readValue(response.toString(), Map.class);
            if(rMap.get("errorCode")!=null) {
            	
            }
            log.info("rMap =>{}",rMap);
            return rMap;
        } catch (Exception e) {
            log.error("error=>{}",e);
        }
        return null;
	}
	
//	private Object translationTest(String target, String source, String text) {
//	private Map<String,Object> translationTest(Map<String,String> param) {
//		String clientId = "bW29mxN17qCeRXGSallE";//애플리케이션 클라이언트 아이디값";
//        String clientSecret = "3Qdwv7AfGY";//애플리케이션 클라이언트 시크릿값";
//        try {
//            text = URLEncoder.encode(text, "UTF-8");
//            String apiURL = "https://openapi.naver.com/v1/papago/n2mt";
//            URL url = new URL(apiURL);
//            HttpURLConnection con = (HttpURLConnection)url.openConnection();
//            con.setRequestMethod("POST");
//            con.setRequestProperty("X-Naver-Client-Id", clientId);
//            con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
//            // post request
//            String postParams = "source=" + source + "&target=" + target + "&text=" + text;
//            con.setDoOutput(true);
//            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//            wr.writeBytes(postParams);
//            wr.flush();
//            wr.close();
//            int responseCode = con.getResponseCode();
//            BufferedReader br;
//            if(responseCode==200) { // 정상 호출
//                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
//            } else {  // 에러 발생
//                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
//            }
//            String inputLine;
//            StringBuffer response = new StringBuffer();
//            while ((inputLine = br.readLine()) != null) {
//                response.append(inputLine);
//                System.out.println("inputLine : " + inputLine);
//            }
//            ObjectMapper om = new ObjectMapper();
//            br.close();
//            
//            return response.toString();
//        } catch (Exception e) {
//            log.error("error=>{}",e);
//        }
//        return null;
//	}
	
	private static Object jsonTest() {
		String res = "{\"message\":{\"@type\":\"response\",\"@service\":\"naverservice.nmt.proxy\",\"@version\":\"1.0.0\",\"result\":{\"srcLangType\":\"ko\",\"tarLangType\":\"en\",\"translatedText\":\"fuck\"}}}";
		ObjectMapper om = new ObjectMapper();
		Map<String, Object> rMap = null;
		try {
			rMap = om.readValue(res, Map.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(rMap);
		Map<String,Map<String,String>> asd = (Map<String, Map<String, String>>) rMap.get("message");
		System.out.println(asd);
		Map<String,String> asd2 = asd.get("result");
		System.out.println(asd2);
		String asd3 = asd2.get("translatedText");
		System.out.println(asd3);
	
		return null;
	}
	
	private String getTranslatedText(String res) {
		if(res!=null) {
			ObjectMapper om = new ObjectMapper();
			Map<String, Object> rMap = null;
			try {
				rMap = om.readValue(res, Map.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			log.debug("rMap => {}", rMap);
			Map<String,Map<String,String>> jsonMapMap = (Map<String, Map<String, String>>) rMap.get("message");
			Map<String,String> jsonMap = jsonMapMap.get("result");
			String jsonString = jsonMap.get("translatedText");
			
			return jsonString; 
			
		}
		return null;
		
	}
	
	
	public static void main(String[] args) {
		jsonTest();
	}
}
