package com.osf.sp.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

@Repository
public class NaverTranslationDAO {
	
	@Resource
	private SqlSession ss;
	
	public List<Map<String,Object>> selectTranslationHisList(){
		return ss.selectList("com.osf.sp.mapper.NaverTransLationMapper.selectList");
	}
	
	public Map<String,Object> selectTranslationHisOne(Map<String,String> param){
		return ss.selectOne("com.osf.sp.mapper.NaverTransLationMapper.selectOne", param);
	}
	
	public Integer insertTranslationHisOne(Map<String,String> param) {
		return ss.insert("com.osf.sp.mapper.NaverTransLationMapper.insertOne", param);
	}
	public Integer updateCountOne(Map<String,Object> param) {
		return ss.update("com.osf.sp.mapper.NaverTransLationMapper.updateCount",param);
	}
	
	public List<Map<String,Object>> selectTransLangList(){
		return ss.selectList("com.osf.sp.mapper.NaverTransLationMapper.selectLangList");
	}
}
